<!DOCTYPE html>
<html>
<head>
	<title>Inverter</title>

<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">script
	
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


      <script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd',minDate: 0 });
  });
  </script>


</head>
<body>
<?php include("header.inc.php");?>
<div class="container">
<div class="vv-center">
		<div class="topic-sm">
		ADD NEW INVERTER
	</div>
		<div class="form-group">
			<label for="productName">Product Name:</label>
			<input type="text" class="form-control" name="product_name" placeholder="Enter the name of product" value="<?php if (isset($line['product_name'])) echo $line['product_name']; ?>">
		</div>

		<div class="form-group">
			<label for="Quantity">Quantity:</label>
			<input type="text" class="form-control tech_inv" id="Quantity" placeholder="Enter the Number of suplies" value="<?php if (isset($line['quantity'])) echo $line['quantity']; ?>" name="quantity">
		</div>

		<div class="form-group">
			<label for="Price">Price:</label>
			<input type="text" name='price' class="form-control tech_inv" id="Price" placeholder="Enter the price in NRs" value="<?php if (isset($line['price'])) echo $line['price']; ?>">
		</div>

		<div class="form-group">
            <label for="Available-by">Available by:</label> 
            <input type="text" name="date" class="form-control" id="datepicker" value="<?php if (isset($line['available_date'])) echo $line['available_date']; ?>"> 
        </div>

		<div class="topic">
			 <h1>Inverter </h1>
		</div>
		
				
		<div class="form-group">
			<label class="topic-sm">Model:</label> 
			<input class='form-control' type="text" name="model" value="<?php if (isset($line['model'])) echo $line['model']; ?>">
		</div>
				
		<div class="form-group">
			<label class="topic-sm">Brand: </label>
			<input class='form-control' type="text" name="brand" value="<?php if (isset($line['brand'])) echo $line['brand']; ?>">
		</div>
			
		<div class="form-group">
			<h2>Electrical specifications - Inverter</h2> 
					
		</div>

		<div class="form-group">
	
				<label for="op_pow_25">Output power (continuous) at 25<sup>o</sup>C: </label>   <input type="text" class="form-control tech_inv" value=""><span>W</span>
		</div>
		<div class="form-group">
	
			<label for="op_pow_30">Output power (30 min) at 25<sup>o</sup>C: </label>  <input type="text" class="form-control tech_inv" value=""> <span>W</span>
		</div>
		<div class="form-group">
	
			<label for="op_poe_5">Output power (5 sec) at 25<sup>o</sup>C: </label> <input type="text" class="form-control tech_inv" value=""> <span>W</span>
		</div>
		<div class="form-group">
	
			<label for="max_op">Maximum output current </label><input type="text" class="form-control tech_inv" value=""> <span>A</span>
		</div>
		<div class="form-group">
	
			<label for="op_freq">Output frequency </label><input type="text" class="form-control tech_inv" value=""> <span>Hz</span>
		</div>
		<div class="form-group" class="tech_inv" value="">
	
			<label for="op_vol">Output voltage </label><input type="text" class="form-control tech_inv" value=""> <span>Vac</span>
		</div>
		<div class="form-group">
	
			<label for="op_wav">Output wave form </label><input type="text" class="form-control tech-inv" value="">
		</div>
		<div class="form-group">
	
			<label for="opt_eff">Optimal efficiency </label><input type="text" class="form-control tech_inv" value=""> <span>W</span>
		</div>
		<div class="form-group double">
	
			<label for="vol_dc">Input DC voltage range </label><input type="text" class="tech_inv" value=""> to <input type="text" class="tech_inv"> <span>Vdc</span>
		</div>
		<div class="form-group">
	
			<label for="cur_dc_max">Maximum input DC current </label><input type="text" class="form-control tech_inv" value=""> <span>A</span>
		</div>
		<div class="form-group">
			<h2>Electrical specification - Charger</h2>
		</div>

		<div class="form-group">
			<label for="op_chrg_cur">Output charge current  </label><input type="text" class="form-control tech_inv" value=""> <span>A</span>
		</div>

		<div class="form-group">
			<label for="nom_op_vol">Nominal output voltage </label><input type="text" class="form-control tech_inv" value=""> <span>Vdc</span>
		</div>

		<div class="form-group triple">
			<label for="op_vol_rng">Output voltage range </label><input type="text" class="form-control tech_inv" value=""> to <input type="text" class="tech_inv" value=""> <span>Vdc</span>
		</div>

		<div class="form-group">
			<label for="chr_con">Charge Control </label><input type="text" class="form-control tech_inv" value=""> <span>stage</span>
		</div>

		<div class="form-group">
			<label for="opt_eff">Optimal efficiency </label><input type="text" class="form-control tech_inv" value=""> <span>%</span>
		</div>

		<div class="form-group">
			<label for="pow_fac">AC input power factor </label><input type="text" class="form-control tech_inv" value="">
		</div>
		<div class="form-group">
			<label for="ip_cur">Input current (fixed/selectable)</label> 
			<select name="inp_crr">
                              <option value ="0"> Select year </option>
                              <option value ="1" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==1 ? 'selected' : ''); } ?>> 1 </option>
                              <option value ="2" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==2 ? 'selected' : ''); } ?>> 2 </option>
                              <option value ="3" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==3 ? 'selected' : ''); } ?>> 3 </option>
                               <option value ="4" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==4 ? 'selected' : ''); } ?>> 4 </option>
                              <option value ="5" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==5 ? 'selected' : ''); } ?>> 5 </option>
                              <option value ="6" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==6 ? 'selected' : ''); } ?>> 6 </option>
                               <option value ="7" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==7 ? 'selected' : ''); } ?>> 7 </option>
                                 <option value ="8" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==8 ? 'selected' : ''); } ?>> 8 </option>
                                 <option value ="9" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==9 ? 'selected' : ''); } ?>> 9 </option> 
                                  <option value ="10" <?php if (isset($line['inp_crr'])) { echo  ($line['inp_crr'] ==10 ? 'selected' : ''); } ?>> 10 </option>                           
                           </select> 
		</div>

		<div class="form-group">
			<label for="ip_ac_vol">Input AC voltage </label><input type="text" class="form-control tech_inv" value=""> <span>Vac</span>
		</div>

		<div class="form-group">
			<label for="ip_ac_vol_rng">Input AC voltage range line to neutral </label>
			<input type="text" class="form-group.double tech_inv" value=""> to 
			<input type="text" class="form-group.double tech_inv" value=""> <span>Vac</span>
		</div>
	







		<div class="form-group">
			<h2>General Specification</h2>
		</div>
		<div>
			<label for="compt_bat_typ">Compatible battery types:</label>
			<ul class="form-group indented">
			<li>
				<input type="checkbox" value=""> FLA 
			</li>
			<li>
				<input type="checkbox" value=""> Gel
			</li>
			<li>
				<input type="checkbox" value=""> AGM
			</li>
			</ul>
		</div>

		<div class="form-group ">
			<label for="trns_rat">Transfer time (AC to inverter and inverter to AC) </label><input type="text" class="form-control tech_inv" value="">  <span>ms</span>
		</div>

		<div class="form-group ">
			<label for="opt_opr_temp_rng">Optimal operating temperature range </label><input type="text" class="form-group.double tech_inv" value=""> <sup>o</sup>C to <input type="text" class="form-group.double tech_inv" value=""> <sup>o</sup>C
		</div>
		<div class="form-group ">
			<label for="alt_opr">Altitude (operating) </label><input type="text" class="form-control tech_inv" value=""> <span>m</span>
		</div>

		<div class="form-group ">
			<label for="pro_wt">Product weight </label><input type="text" class="form-control tech_inv" value=""> <span>Kg</span>
		</div>

		<div class="form-group triple ">
			<label for="pro_dim">Product dimensions (H x W x D) </label><input type="text" class="tech_inv form-group triple" value=""> x <input type="text" class="tech_inv form-group.triple" value=""> x <input type="text" class="tech_inv form-group.triple" value="">
		</div>
		<div class="form-group ">
			<label for="sys_net_rem_monit">System network and remote monitoring </label><input type="radio" class="tech-inv" value=""> Available <input type="radio" class="tech-inv" value=""> Not Available
		</div>
		<br>
		<div class="form-group ">
			<label for="wrr">Warranty</label>
			<div class="form-group ">
                           <select name="wrr_year">
                              <option value ="0"> Select year </option>
                              <option value ="1" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==1 ? 'selected' : ''); } ?>> 1 </option>
                              <option value ="2" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==2 ? 'selected' : ''); } ?>> 2 </option>
                              <option value ="3" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==3 ? 'selected' : ''); } ?>> 3 </option>
                               <option value ="4" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==4 ? 'selected' : ''); } ?>> 4 </option>
                              <option value ="5" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==5 ? 'selected' : ''); } ?>> 5 </option>
                              <option value ="6" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==6 ? 'selected' : ''); } ?>> 6 </option>
                               <option value ="7" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==7 ? 'selected' : ''); } ?>> 7 </option>
                                 <option value ="8" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==8 ? 'selected' : ''); } ?>> 8 </option>
                                 <option value ="9" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==9 ? 'selected' : ''); } ?>> 9 </option> 
                                  <option value ="10" <?php if (isset($line['wrr_year'])) { echo  ($line['wrr_year'] ==10 ? 'selected' : ''); } ?>> 10 </option>                           
                           </select>
                              year	
                              <select name="wrr_month">
                                 <option value ="0"> Select month</option>
                                 <option value ="1" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==1 ? 'selected' : ''); } ?>> 1 </option>
                                 <option value ="2" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==2 ? 'selected' : ''); } ?>> 2 </option>
                                 <option value ="3" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==3 ? 'selected' : ''); } ?>> 3 </option>
                                 <option value ="4" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==4 ? 'selected' : ''); } ?>> 4 </option>
                                 <option value ="5" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==5 ? 'selected' : ''); } ?>> 5 </option>
                                 <option value ="6" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==6 ? 'selected' : ''); } ?>> 6 </option>
                                 <option value ="7" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==7 ? 'selected' : ''); } ?>> 7 </option>
                                 <option value ="8" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==8 ? 'selected' : ''); } ?>> 8 </option>
                                 <option value ="9" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==9 ? 'selected' : ''); } ?>> 9 </option>
                                 <option value ="10" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==10 ? 'selected' : ''); } ?>> 10 </option>
                                 <option value ="11" <?php if (isset($line['wrr_month'])) { echo  ($line['wrr_month'] ==11 ? 'selected' : ''); } ?>> 11 </option>
                              </select>
                              month
                          </div>
		</div>

		<div class="form-group ">
			<label for="saf_reg_app">Safety Regulatory Approvals:</label>
			<ul class="indented">
			<li>
				<input type="checkbox" value=""> NEPQA Compliant 
			</li>
			<li>
				<input type="checkbox" value=""> OTHER <input type="text" value="">
			</li>
			
			</ul>
		</div>

	<div class="form-group">
	<input class="btn btn-success" type="submit" name="submit" value="submit">
	</div>
	
</div>
	<?php include("footer.inc.php");?>
</div>
</body>
<script type="text/javascript">

$(document).ready( function (){

	$('.tech_inv').keypress(
		function(e){
			dontAllowNumbers(e); 
		}); 
	
	$('.chk_int').keypress(function(e){dontAllowNumbers(e); }); 
	
	});



function dontAllowNumbers(e){


		var typed = (e.charCode);


		// 47-57 is number
		if( typed < 46 || typed > 57){
			e.preventDefault();
			console.log('Numbers Blocked');
			
		
	}
}
</script>
</html>