<?php
session_start(); 
require_once("config/config.inc.php");
require_once("config/functions.inc.php");
	validate_admin();
	@extract($_POST);
    $prg_id=$_SESSION['sess_prg_id'];   

    if (isset($prg_id))
    {
            $sql="SELECT * FROM  tbl_offer_msg where prg_id='$prg_id'"; 
	        $result=executeQuery($sql);	     

    }


?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php SITE_ADMIN_TITLE ?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>

		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
		<style type="text/css">
		div.dataTables_wrapper 
		div.dataTables_filter input {
		width: auto !important;
		}

		#datatb_wrapper {
			overflow: hidden !important;
		}
		</style>
 
		<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#datatb').DataTable({
					//"order": [[ 3, "desc" ]],
					"columnDefs": [ {
					"targets": [ 0, 2 ],
					"orderable": false
					} ],
					 "pageLength": 50
					});
			} );
		</script>
</head>
<body>
<?php include("header.inc.php");?>
<div class="container"> 		
	<!-- Center Part Begins Here  -->
	<div class="vv-center">
		<div class="title-info">
		<h2>Offer Details</h2>
		
	</div>
		
		<div class='form-wrapper'>
			
			 <div class="table-responsive">          
			  <table class="table" id="datatb">
			    <thead>
			      <tr>
			        <th>S.No</th>
			        <th>Set Message</th>			        
			        <th>Set Range</th>
			        <th>Status</th>       
			      </tr>
			    </thead>
			    <tbody>
				<?php
					$i =1;					
					while($rc = mysql_fetch_array($result)) { ?>
					<tr>
					<td><?php echo $i; ?></td>
					<td><a href="eg_offer_msg.php?id=<?php echo $rc['id'];?>" class="orange_link">Edit</a></td>
					<td><a href="eg_offer_range.php?id=<?php echo $rc['id'];?>" class="orange_link">Edit</a></td>					
					<td><?php if($rc['status']==1){?> Active <?php }else{?>Inactive<?php }?></td>
					</tr>
				<?php $i++; } ?>
			    </tbody>
			  </table>
			  </div>

		</div>
		
	</div> 				
</div>

<?php include("footer.inc.php");?>

</body>
</html>
<?php
$_SESSION['sess_msg'] = '';
?>
