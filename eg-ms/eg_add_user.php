<?php
session_start(); 
require_once("config/config.inc.php");
require_once("config/functions.inc.php");
	validate_admin();
	@extract($_POST);
    $prg_id=$_SESSION['sess_prg_id'];   
	if (isset($_GET['id'])) {
		$id=$_GET['id'];
		$sql="SELECT * FROM tbl_user where usr_id=".$id; 
		$result=executeQuery($sql);
		$num=mysql_num_rows($result);
		if($line=ms_stripslashes(mysql_fetch_array($result))){
			@extract($line); 
		}
		
	 }

?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php SITE_ADMIN_TITLE ?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>
<link rel="stylesheet" href="lib/sweetalert-master/dist/sweetalert.css">
<link rel="stylesheet" href="lib/sweetalert-master/example/example.css">
<script type="text/javascript" src="lib/sweetalert-master/dist/sweetalert-dev.js"></script>

<script language="JavaScript" type="text/JavaScript">
var msg = "Kindly enter the following details.\n";
function validateForm(obj)
{  

  var str="";
	
	if(obj.user_name.value == '') str+='Please Enter User Name. \n';
	if(obj.password.value == '') str+='Please Enter Password. \n';
	if(obj.email.value == '') str+='Please Enter Email. \n';
	if(obj.phone.value == '') str+='Please Enter Phone Number. \n'; 

	if(obj.password.value != obj.confirm_password.value) {
		alert('Confirm Password does not match.');
		return false;
	}

	if(str) {
		//alert(msg+str);  
    sweetAlert("Oops...", msg+str, "error");
		return false;
	}
}
</script>

</head>
<body>
<?php include("header.inc.php");?>
<div class="container"> 		
	<!-- Center Part Begins Here  -->
	<div class="vv-center">
	
		<div class='form-wrapper'>
			<p align="center" class="warning"><?php echo (isset($_SESSION['sess_msg']) ? $_SESSION['sess_msg'] : ''); ?></p>
			<button onclick="window.location.href='eg_user_list.php'" type="button" class="btn btn-info" id="show_login">Back</button>	
			<div id = "loginform">
			 <form role="form" action="auth/add_eg_user.php" method="POST" onsubmit="return validateForm(this)">
			   <div>
			 	<div>

				<?php  echo (isset($id) ? 'Edit' : 'Add'); ?> User
				</div> 
				<!-- eo top -->
				<input type="hidden" name="submitForm" value="yes">
				<input type="hidden" name="id" value="<?php if (isset($_GET['id'])) echo $line['usr_id']; ?>">

				<div>
				User Name: <input type="text" class="form-control" name="user_name" value="<?php if (isset($_GET['id'])) echo $line['usr_name']; ?>"> 

				</div>

				<div>
				Password: <input type="text" class="form-control" name="password" value="<?php if (isset($_GET['id'])) echo $line['password']; ?>"> 

				</div>
				<div>
				Confirm Password: <input type="text" class="form-control" name="confirm_password" value="<?php if (isset($_GET['id'])) echo $line['password']; ?>"> 

				</div>
				<div>
				Phone: <input type="text" name="phone" class="form-control" value="<?php if (isset($_GET['id'])) echo $line['usr_phone']; ?>"> 
				</div>

				<div>
				Email: <input type="text" name="email" class="form-control" value="<?php if (isset($_GET['id'])) echo $line['usr_email']; ?>"> 
				</div><br>

				<input type="submit" name="submit" class="btn btn-success" value="submit">
				</div>			    
			  </form>
        </div>		 

		</div>
		
	</div> 				
</div>

<?php include("footer.inc.php");?>
</body>
</html>
<?php
$_SESSION['sess_msg'] = '';
?>
