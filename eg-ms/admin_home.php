<?php
session_start();
require_once("config/config.inc.php");
require_once("config/functions.inc.php");
validate_admin();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php SITE_ADMIN_TITLE ?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<?php include("header.inc.php");?>
<div class="container">
	<img class='dash-img' src="img/bajeko_dashboard.jpg">

<!--     <div class="p-btn-holder">
    <b>Welcome to <?php if($_SESSION['type']==1) { ?>Administration <?php }else{?>Seller<?php }?> Suite</b><br>
    Please use the navigation links on the top to access 
    different sections of the main a <?php if($_SESSION['type']==1) { ?>Administration <?php }else{?>Seller<?php }?> Suite.<br />
    </div> -->
</div>	 
<?php include("footer.inc.php");?>
</body>
</html>
