$(document).ready(function() {

    $("#login").click(function() {


        var email = $("#email").val();
        var password = $("#password").val();

        //checking for blank fields
        if (email == '' || password == '') {
            $('input[type="text"],input[type="password"]').css("border", "2px solid red");
            $('input[type="text"],input[type="password"]').css("box-shadow", "0 0 3px red");
            //alert("Please fill all fields...!!!!!!");
        } else {
            $.post("verify.php", {
                    email1: email,
                    password1: password
                },
                function(data) { 
                    if (data == 'Invalid Email.......') {
                        $('input[type="text"]').css({
                            "border": "2px solid red",
                            "box-shadow": "0 0 3px red"
                        });
                        $('input[type="password"]').css({
                            "border": "2px solid #00F5FF",
                            "box-shadow": "0 0 5px #00F5FF"
                        });
                        //alert(data);

                        swal({
                                title: "Invalid Email....... !",
                                text: "",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#333',
                                confirmButtonText: 'OK',
                                //cancelButtonText: "No, cancel plx!",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = 'index.php';
                                } else {
                                    window.location = 'index.php';
                                }
                            });

                    } else if (data == 404) {
                        $('input[type="text"],input[type="password"]').css({
                            "border": "2px solid red",
                            "box-shadow": "0 0 3px red"
                        });
                        swal({
                                title: "Email or Password is wrong...!!!!",
                                text: "",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: '#333',
                                confirmButtonText: 'OK',
                                //cancelButtonText: "No, cancel plx!",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = 'index.php';
                                } else {
                                    window.location = 'index.php';
                                }
                            });
                    } else if (data == 2255) {
                        $("form")[0].reset();
                        $('input[type="text"],input[type="password"]').css({
                            "border": "2px solid #00F5FF",
                            "box-shadow": "0 0 5px #00F5FF"
                        });

                        //alert(data);
                        var url = "admin_home.php";
                        $(location).attr('href', url);
                    } else {
                        //alert(data);
                    }

                });
        }

    });

});