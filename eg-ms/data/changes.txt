//16 feb
ALTER TABLE `tbl_charge` ADD `gen_nepqa` VARCHAR( 100 ) NOT NULL AFTER `wrrr_lmt` 
ALTER TABLE `tbl_panel` ADD `gen_nepqa` VARCHAR( 100 ) NOT NULL AFTER `wrr_lim` 



//15 feb
ALTER TABLE  `tbl_battery` ADD  `product_type_id` INT( 11 ) NOT NULL AFTER  `product_id` ;
ALTER TABLE  `tbl_product` ADD  `product_type_id` INT( 11 ) NOT NULL AFTER  `url` ;

ALTER TABLE  `tbl_battery` ADD  `gen_nepqa` VARCHAR( 100 ) NOT NULL AFTER  `wrr_lmt` ;
ALTER TABLE  `tbl_battery` ADD  `wrr_year` INT( 11 ) NOT NULL AFTER  `gen_nepqa` ;
ALTER TABLE  `tbl_battery` ADD  `wrr_month` INT( 11 ) NOT NULL AFTER  `wrr_year` ;
ALTER TABLE  `tbl_battery` CHANGE  `capacity`  `capacity` VARCHAR( 100 ) NOT NULL ;



//6 jfeb 2016 "modify type : pradeep"
ALTER TABLE  `tbl_charge` CHANGE  `type`  `type` VARCHAR( 50 ) NOT NULL ;
ALTER TABLE  `tbl_charge` CHANGE  `wrrr_lmt`  `wrrr_lmt` VARCHAR( 100 ) NULL ;





ALTER TABLE  `tbl_enquiry` ADD  `phone_no` BIGINT( 25 ) NOT NULL AFTER  `name` ;

ALTER TABLE  `tbl_charge` ADD  `cert_chk` VARCHAR( 50 ) NULL AFTER  `cert` ;
ALTER TABLE  `tbl_battery` ADD  `cert_chk` VARCHAR( 50 ) NULL AFTER  `certf` ;
