-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2016 at 07:53 AM
-- Server version: 5.1.54-community-log
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saral-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `sb_users`
--

CREATE TABLE IF NOT EXISTS `sb_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `sb_users`
--

INSERT INTO `sb_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`, `type`) VALUES
(11, 'user', 'user', 'user', 'user@gmail.com', '', '0000-00-00 00:00:00', '', 1, '', 2),
(12, 'admin', 'admin', 'admin', 'admin', '', '0000-00-00 00:00:00', '', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_battery`
--

CREATE TABLE IF NOT EXISTS `tbl_battery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `certf` varchar(200) NOT NULL,
  `cert_chk` varchar(50) DEFAULT NULL,
  `nm_voltage` int(11) NOT NULL,
  `capacity` varchar(100) NOT NULL,
  `cap_ah` varchar(100) NOT NULL,
  `opr_lc_dod1` int(11) NOT NULL,
  `opr_lc_cyc1` int(11) NOT NULL,
  `opr_lc_dod2` int(11) NOT NULL,
  `opr_lc_cyc2` int(11) NOT NULL,
  `dms_h` int(11) NOT NULL,
  `dms_w` int(11) NOT NULL,
  `dms_d` int(11) NOT NULL,
  `dim_pam` varchar(50) DEFAULT NULL,
  `weg_dry` int(11) NOT NULL,
  `weg_acid` int(11) NOT NULL,
  `wrr_date` varchar(200) NOT NULL,
  `wrr_lmt` varchar(200) NOT NULL,
  `gen_nepqa` varchar(100) NOT NULL,
  `wrr_year` int(11) NOT NULL,
  `wrr_month` int(11) NOT NULL,
  `wrr_trm_cov` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_battery`
--

INSERT INTO `tbl_battery` (`id`, `product_id`, `product_type_id`, `model`, `brand`, `type`, `certf`, `cert_chk`, `nm_voltage`, `capacity`, `cap_ah`, `opr_lc_dod1`, `opr_lc_cyc1`, `opr_lc_dod2`, `opr_lc_cyc2`, `dms_h`, `dms_w`, `dms_d`, `dim_pam`, `weg_dry`, `weg_acid`, `wrr_date`, `wrr_lmt`, `gen_nepqa`, `wrr_year`, `wrr_month`, `wrr_trm_cov`) VALUES
(12, 147, 1, '9', '9', 'mat', '9', '', 9, 'C-10', '9', 9, 9, 9, 8, 9, 9, 9, NULL, 9, 9, '', '23', '', 9, 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_charge`
--

CREATE TABLE IF NOT EXISTS `tbl_charge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `cert` varchar(255) NOT NULL,
  `cert_chk` varchar(50) DEFAULT NULL,
  `tec_cpt_amp` int(11) NOT NULL,
  `tec_cpt_amp_1` int(11) NOT NULL,
  `tec_cpt_amp_2` int(11) NOT NULL,
  `tec_cpt_amp_3` int(11) NOT NULL,
  `tec_nom_volt` int(11) NOT NULL,
  `tech_bvor_vdc_from` int(11) NOT NULL,
  `tech_bvor_vdc_to` int(11) NOT NULL,
  `tech_max_pv_volt` int(11) NOT NULL,
  `tech_max_pv_open_cir_volt` int(11) NOT NULL,
  `tech_max_pv_short_cir_volt` int(11) NOT NULL,
  `tech_wire_size_from` int(11) NOT NULL,
  `tech_wire_size_to` int(11) NOT NULL,
  `tech_max_out_power` int(11) NOT NULL,
  `tech_charger_regulation_method` varchar(255) NOT NULL,
  `tech_supported_battery_types` varchar(255) NOT NULL,
  `tech_max_eff` int(11) NOT NULL,
  `tech_pow_night_time` int(11) NOT NULL,
  `tech_battery_sensor` int(11) NOT NULL,
  `tech_auxiliary_output_v_from` int(11) NOT NULL,
  `tech_auxiliary_output_v_to` int(11) NOT NULL,
  `tech_auxiliary_output_ma` int(11) NOT NULL,
  `tech_product_weg` int(11) NOT NULL,
  `tech_product_d_h` int(11) NOT NULL,
  `tech_product_d_d` int(11) NOT NULL,
  `tech_product_d_w` int(11) NOT NULL,
  `tech_air_tmp_from` int(11) NOT NULL,
  `tech_air_tmp_to` int(11) NOT NULL,
  `wrr_year` varchar(100) NOT NULL,
  `wrr_month` varchar(100) NOT NULL,
  `wrrr_lmt` varchar(100) DEFAULT NULL,
  `gen_nepqa` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_charge`
--

INSERT INTO `tbl_charge` (`id`, `product_id`, `model`, `brand`, `type`, `cert`, `cert_chk`, `tec_cpt_amp`, `tec_cpt_amp_1`, `tec_cpt_amp_2`, `tec_cpt_amp_3`, `tec_nom_volt`, `tech_bvor_vdc_from`, `tech_bvor_vdc_to`, `tech_max_pv_volt`, `tech_max_pv_open_cir_volt`, `tech_max_pv_short_cir_volt`, `tech_wire_size_from`, `tech_wire_size_to`, `tech_max_out_power`, `tech_charger_regulation_method`, `tech_supported_battery_types`, `tech_max_eff`, `tech_pow_night_time`, `tech_battery_sensor`, `tech_auxiliary_output_v_from`, `tech_auxiliary_output_v_to`, `tech_auxiliary_output_ma`, `tech_product_weg`, `tech_product_d_h`, `tech_product_d_d`, `tech_product_d_w`, `tech_air_tmp_from`, `tech_air_tmp_to`, `wrr_year`, `wrr_month`, `wrrr_lmt`, `gen_nepqa`) VALUES
(1, 128, 'q1q1qqqq1', 'q1qq1q1q', 'pwm', '', 'y', 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2147483647, '', '', 111, 231234, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, '1', '1', 'dddddd', 'y'),
(2, 148, '8', '8', 'mppt', 'other', 'y', 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, '', '', 8, 8, 0, 8, 8, 8, 8, 8, 8, 8, 8, 8, '8', '8', '8', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_charge_regulation`
--

CREATE TABLE IF NOT EXISTS `tbl_charge_regulation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `charge` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_charge_regulation`
--

INSERT INTO `tbl_charge_regulation` (`id`, `product_id`, `charge`) VALUES
(17, 128, 'bulk'),
(18, 128, 'absrp'),
(20, 148, 'bulk');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_charge_sup_batt`
--

CREATE TABLE IF NOT EXISTS `tbl_charge_sup_batt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `sup_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_charge_sup_batt`
--

INSERT INTO `tbl_charge_sup_batt` (`id`, `product_id`, `sup_type`) VALUES
(17, 128, 'agm'),
(18, 128, 'gel'),
(20, 148, 'fld');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_enquiry`
--

CREATE TABLE IF NOT EXISTS `tbl_enquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone_no` bigint(25) NOT NULL,
  `email` varchar(200) NOT NULL,
  `descp` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_enquiry`
--

INSERT INTO `tbl_enquiry` (`id`, `name`, `phone_no`, `email`, `descp`) VALUES
(7, '', 0, '', '   ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inverter`
--

CREATE TABLE IF NOT EXISTS `tbl_inverter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `model` varchar(100) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `esi_opc` int(11) NOT NULL,
  `esi_op_30` int(11) NOT NULL,
  `esi_op_5` int(11) NOT NULL,
  `esi_moc` int(11) NOT NULL,
  `esi_of` int(11) NOT NULL,
  `esi_ov` int(11) NOT NULL,
  `esi_owf` varchar(100) NOT NULL,
  `esi_oe` int(11) NOT NULL,
  `esi_dcv_from` int(11) NOT NULL,
  `esi_dcv_to` int(11) NOT NULL,
  `esi_max_dc_cur` int(11) NOT NULL,
  `esc_occ` int(11) NOT NULL,
  `esc_nov` int(11) NOT NULL,
  `esc_ovr_from` int(11) NOT NULL,
  `esc_ovr_to` int(11) NOT NULL,
  `esc_cc` int(11) NOT NULL,
  `esc_oe` int(11) NOT NULL,
  `esc_ac_pow` int(11) NOT NULL,
  `esc_ic` varchar(100) NOT NULL,
  `esc_ac_volt` int(11) NOT NULL,
  `esc_volt_rang_from` int(11) NOT NULL,
  `esc_volt_rang_to` int(11) NOT NULL,
  `gen_tt` int(11) NOT NULL,
  `gen_optr_from` int(11) NOT NULL,
  `gen_optr_to` int(11) NOT NULL,
  `gen_allt` int(11) DEFAULT NULL,
  `gen_pw` int(11) NOT NULL,
  `gen_dem_h` int(11) NOT NULL,
  `gen_dem_w` int(11) NOT NULL,
  `gen_dem_d` int(11) NOT NULL,
  `gen_snmn` varchar(50) NOT NULL,
  `wrr_year` varchar(50) NOT NULL,
  `wrr_month` varchar(50) NOT NULL,
  `gen_nepqa` varchar(50) NOT NULL,
  `gen_other` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_inverter`
--

INSERT INTO `tbl_inverter` (`id`, `product_id`, `model`, `brand`, `esi_opc`, `esi_op_30`, `esi_op_5`, `esi_moc`, `esi_of`, `esi_ov`, `esi_owf`, `esi_oe`, `esi_dcv_from`, `esi_dcv_to`, `esi_max_dc_cur`, `esc_occ`, `esc_nov`, `esc_ovr_from`, `esc_ovr_to`, `esc_cc`, `esc_oe`, `esc_ac_pow`, `esc_ic`, `esc_ac_volt`, `esc_volt_rang_from`, `esc_volt_rang_to`, `gen_tt`, `gen_optr_from`, `gen_optr_to`, `gen_allt`, `gen_pw`, `gen_dem_h`, `gen_dem_w`, `gen_dem_d`, `gen_snmn`, `wrr_year`, `wrr_month`, `gen_nepqa`, `gen_other`) VALUES
(2, 150, '9', '9', 9, 9, 9, 9, 9, 9, '9', 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, '2', 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 'ava', '8', '1', 'y', 'other');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inverter_cbt`
--

CREATE TABLE IF NOT EXISTS `tbl_inverter_cbt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `cbt_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_inverter_cbt`
--

INSERT INTO `tbl_inverter_cbt` (`id`, `product_id`, `cbt_id`) VALUES
(1, 150, 'Gel'),
(2, 150, 'Gel'),
(3, 150, 'Gel');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_panel`
--

CREATE TABLE IF NOT EXISTS `tbl_panel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `tech_capacity` int(11) DEFAULT NULL,
  `tech_pmax` int(11) DEFAULT NULL,
  `tech_vmp` int(11) DEFAULT NULL,
  `tech_imp` int(11) DEFAULT NULL,
  `tech_voc` int(11) DEFAULT NULL,
  `tech_isc` int(11) DEFAULT NULL,
  `tech_module_efficiency` int(11) DEFAULT NULL,
  `tech_cell_efficiency` int(11) DEFAULT NULL,
  `tech_opr_temp_from` int(11) DEFAULT NULL,
  `tech_opr_temp_to` int(11) DEFAULT NULL,
  `tech_max_sys_vol_iec` int(11) DEFAULT NULL,
  `tech_max_sys_vol_ul` int(11) DEFAULT NULL,
  `tech_max_fuse_rating` int(11) DEFAULT NULL,
  `tech_app_clf` varchar(200) DEFAULT NULL,
  `tech_pw_tolerance` int(11) DEFAULT NULL,
  `tech_dmsH` int(11) DEFAULT NULL,
  `tech_dms_w1` int(11) DEFAULT NULL,
  `tech_dms_d1` int(11) DEFAULT NULL,
  `dim_pam` varchar(100) NOT NULL,
  `tech_weight` int(11) DEFAULT NULL,
  `cert_other` varchar(255) DEFAULT NULL,
  `wrr_year` int(11) NOT NULL,
  `wrr_month` int(11) NOT NULL,
  `wrr_trms` int(11) DEFAULT NULL,
  `wrr_lim` varchar(200) DEFAULT NULL,
  `gen_nepqa` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_panel`
--

INSERT INTO `tbl_panel` (`id`, `product_id`, `model`, `brand`, `type`, `tech_capacity`, `tech_pmax`, `tech_vmp`, `tech_imp`, `tech_voc`, `tech_isc`, `tech_module_efficiency`, `tech_cell_efficiency`, `tech_opr_temp_from`, `tech_opr_temp_to`, `tech_max_sys_vol_iec`, `tech_max_sys_vol_ul`, `tech_max_fuse_rating`, `tech_app_clf`, `tech_pw_tolerance`, `tech_dmsH`, `tech_dms_w1`, `tech_dms_d1`, `dim_pam`, `tech_weight`, `cert_other`, `wrr_year`, `wrr_month`, `wrr_trms`, `wrr_lim`, `gen_nepqa`) VALUES
(10, 153, '9', '7', 'mono', 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, '7', 7, 7, 7, 7, 'cm', 7, '7', 7, 7, 77, '7', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_panel_cert`
--

CREATE TABLE IF NOT EXISTS `tbl_panel_cert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `cert_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `panel_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `tbl_panel_cert`
--

INSERT INTO `tbl_panel_cert` (`id`, `product_id`, `cert_id`) VALUES
(49, 146, 'JET'),
(50, 146, 'CE'),
(51, 153, 'IIEC_61215');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `product_type` varchar(100) NOT NULL,
  `available_date` date NOT NULL,
  `url` varchar(255) NOT NULL,
  `product_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=154 ;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `product_name`, `user_id`, `quantity`, `price`, `product_type`, `available_date`, `url`, `product_type_id`) VALUES
(153, '89989898', 11, 7, 9, 'Panel', '0000-00-00', 'product_add_panel.php', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
