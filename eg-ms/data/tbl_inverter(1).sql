-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2016 at 08:32 AM
-- Server version: 5.1.54
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saral-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inverter`
--

CREATE TABLE IF NOT EXISTS `tbl_inverter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `model` varchar(100) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `esi_opc` int(11) NOT NULL,
  `esi_op_30` int(11) NOT NULL,
  `esi_op_5` int(11) NOT NULL,
  `esi_moc` int(11) NOT NULL,
  `esi_of` int(11) NOT NULL,
  `esi_ov` int(11) NOT NULL,
  `esi_owf` varchar(100) NOT NULL,
  `esi_oe` int(11) NOT NULL,
  `esi_dcv_from` int(11) NOT NULL,
  `esi_dcv_to` int(11) NOT NULL,
  `esi_max_dc_cur` int(11) NOT NULL,
  `esc_occ` int(11) NOT NULL,
  `esc_nov` int(11) NOT NULL,
  `esc_ovr` int(11) NOT NULL,
  `esc_cc` int(11) NOT NULL,
  `esc_oe` int(11) NOT NULL,
  `esc_ac_pow` int(11) NOT NULL,
  `esc_ic` varchar(100) NOT NULL,
  `esc_ac_volt` int(11) NOT NULL,
  `esc_volt_rang_from` int(11) NOT NULL,
  `esc_volt_rang_to` int(11) NOT NULL,
  `gen_tt` int(11) NOT NULL,
  `gen_optr_from` int(11) NOT NULL,
  `gen_optr_to` int(11) NOT NULL,
  `gen_allt` int(11) DEFAULT NULL,
  `gen_pw` int(11) NOT NULL,
  `gen_dim_h` int(11) NOT NULL,
  `gen_dim_w` int(11) NOT NULL,
  `gen_dim_d` int(11) NOT NULL,
  `gen_snm` int(11) NOT NULL,
  `gen_wrr_year` int(11) NOT NULL,
  `gen_wrr_month` int(11) NOT NULL,
  `gen_nepqa` varchar(100) NOT NULL,
  `gen_nepqa_other` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_inverter`
--

