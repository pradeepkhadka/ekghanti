<?php
session_start(); 
require_once("config/config.inc.php");
require_once("config/functions.inc.php");
	validate_admin();
	@extract($_POST);
    $prg_id=$_REQUEST['prg_id'];
    $usr_id= get_usrByprg($prg_id);  
    if (isset($prg_id))
    {
            $sql="SELECT * FROM tbl_gems tg where tg.prg_id='$prg_id'"; 
	        $result=executeQuery($sql);
	        $num=mysql_num_rows($result); 
	        
	        if($line=ms_stripslashes(mysql_fetch_array($result)))
	        {
	            @extract($line);
	        }

    }  


?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php SITE_ADMIN_TITLE ?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>
<link rel="stylesheet" href="lib/sweetalert-master/dist/sweetalert.css">
<style type="text/css">
#loginform
{
   width:100%;
   height:170px;
   margin-top:10px;
   background-color:#A94442;
   border-radius:3px;
   box-shadow:0px 0px 10px 0px #424242;
   padding:10px;
   box-sizing:border-box;
   font-family:helvetica;
   visibility:hidden;
   display:none;
}
#loginform #close_login
{
/*   position:absolute;
   top:140px;
   left:735px;
   width:15px;
   height:15px;*/
}
#loginform p
{
   margin-top:40px;
   font-size:22px;
   color:#E6E6E6;
}
#loginform #login
{
   width:250px;
   height:40px;
   border:2px solid silver;
   border-radius:3px;
   padding:5px;
}
#loginform #password
{
   margin-top:5px;
   width:250px;
   height:40px;
   border:2px solid silver;
   border-radius:3px;
   padding:5px;
}
#loginform #dologin
{
   margin-left:-5px;
   margin-top:10px;
   width:250px;
   height:40px;
   border:none;
   border-radius:3px;
   color:#E6E6E6;
   background-color:grey;
   font-size:20px;
}
</style>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="lib/sweetalert-master/dist/sweetalert-dev.js"></script>

<script type="text/javascript">
$(document).ready(function(){

   $("#show_login").click(function(){
    showpopup();
   });
   $("#close_login").click(function(){
    hidepopup();
   });

});


function showpopup()
{
   $("#loginform").fadeIn();
   $("#loginform").css({"visibility":"visible","display":"block"});
}

function hidepopup()
{
   $("#loginform").fadeOut();
   $("#loginform").css({"visibility":"hidden","display":"none"});
}
</script>

<script language="JavaScript" type="text/JavaScript">
var msg = "Kindly enter the following details.\n";
function validateForm(obj)
{  

  var str="";
	
	if(obj.value.value == '') str+='Please Enter Value. \n';
	if(str) {
		//alert(msg+str);  
    sweetAlert("Oops...", msg+str, "error");
		return false;
	}
}
</script>

</head>
<body>
<?php include("header.inc.php");?>
<div class="container"> 		
	<!-- Center Part Begins Here  -->
	<div class="vv-center">
		<div class="title-info">
		<h2>My Gems</h2>
		
	</div>
		
		<div class='form-wrapper'>
			<div>
			<label>Remaining Gems Value : </label>
			<label><?php  echo (isset($line['gems']) ? $line['gems'] : ''); ?>	</label>
			<button type="button" class="btn btn-info" id="show_login">Add Value</button>		
			</div>
			<p align="center" class="warning"><?php echo (isset($_SESSION['sess_msg']) ? $_SESSION['sess_msg'] : ''); ?></p>	
			<div id = "loginform">
			 <form role="form" action="auth/add_eg_gems.php" method="POST" onsubmit="return validateForm(this)">
			 	<input type = "hidden" id = "usr_id" name="usr_id" value = "<?php echo $usr_id; ?>" >
			 	<input type = "hidden" id = "prg_id" name="prg_id" value = "<?php echo $prg_id; ?>" >
			    <div class="form-group">
			      <label for="usr">Enter Value* :</label>
			     <input type = "text" id = "value" name = "value" >
			    </div>  			     
			     <button class='btn btn-primary' name="save" value="save">Save</button>
			     <button type="button" class="btn btn-info" id="close_login">Cancel</button>			    
			  </form>
        </div>

			 <div class="table-responsive">          
			  <table class="table">
			    <thead>
			      <tr>
			        <th>S.No</th>
			        <th>Date</th>
			        <th>Value</th>
			        <th>Program</th>
			        <th>Type</th>       
			      </tr>
			    </thead>
			    <tbody>
				<?php
					$i =1;
					$getDetails = getPrgGems($prg_id);
					while($rc = mysql_fetch_array($getDetails)){ ?>
					<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $rc['date']; ?></td>
					<td><?php echo $rc['value']; ?></td>
					<td><?php echo $rc['prg_id']; ?> </td>
					<td><?php echo $rc['type']; ?> </td>
					</tr>
				<?php $i++; } ?>
			    </tbody>
			  </table>
			  </div>

		</div>
		
	</div> 				
</div>

<?php include("footer.inc.php");?>
</body>
</html>
<?php
$_SESSION['sess_msg'] = '';
?>
