<?php 
session_start();
require_once("../config/config.inc.php");
require_once("../config/functions.inc.php");
@extract($_POST);
validate_admin();
$user_id=$_SESSION['sess_uid'];
$column="select * ";
$sql=" from tbl_enquiry order by id desc";

$sql=$column.$sql;
//$sql.=" order by $order_by $order_by2  limit $start, $pagesize ";
//$sql.=" order by $order_by  limit $start, $pagesize ";
//echo "<br>$sql</br>";
$result=executequery($sql);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=SITE_ADMIN_TITLE?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/dt/dt-1.10.10/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/s/dt/dt-1.10.10/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 1, "desc" ]]
    } );
} );

function checkall(objForm)
{
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++){
		if (objForm.elements[i].type=='checkbox') 
			objForm.elements[i].checked=objForm.check_all.checked;
	}
}

function del_prompt(frmobj,comb)
{
	if(comb=='Delete'){
		if(confirm ("Are you sure you want to delete Record(s)")){
			frmobj.action = "seo_del.php";
			frmobj.submit();
		}
		else{ 
			return false;
		}
	}
	else
	{
		frmobj.action = "seo_del.php";
		frmobj.submit();
	}
	
}
function validate_form(obj)
{alert(obj);
	var msg='Incomplete Data, Kindly fill required fields...\n\n', flag=false;
	
	if(obj.displayName.value == '') msg+='- Please Enter Display Name. \n';
	if(msg == 'Incomplete Data, Kindly fill required fields...\n\n')
		return true;
	else{
		alert(msg);
		return false;
	}
	
}
function change_records(val)
{
	location.href='seo_list.php?p='+val;
}

</script></head>
<body>
<?php include("header.inc.php");?>
<div class="container footer-height">

  <h2>View Enquiry</h2>
            
  <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>Name</th>
        <th>Phone No.</th>
        <th>Email </th>
        <th>Description </th>
      </tr>
    </thead>
    <tbody>
    <?php while($line=mysql_fetch_array($result)) { ?>
      <tr>
        <td><?php echo $line['name']; ?></td>
        <td><?php echo $line['phone_no']; ?></td>
        <td><?php echo $line['email']; ?></td>
        <td><?php echo $line['descp']; ?></td>
       </tr>
      <?php } ?>    

    </tbody>
  </table>
</div>


<?php include("footer.inc.php"); ?>
</body>
</html>
