  <nav class="navbar navbar-defaultb">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img width="130" height="130" src="img/redmud-logo.png"></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        
        <ul class="nav navbar-nav navbar-right">
          
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Settings <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">Change password</a></li>
              <li><a href="logout.php">Log out</a></li>
            </ul>
          </li>
        </ul>

        <ul class="nav navbar-nav">  
        <li><a href="admin_home.php">Dashboard</a></li> 
        <?php $chk = is_admin($_SESSION['l_type']) ;
          if($chk == 1) {
         ?>
        <li><a href="eg_user_list.php">User Manager</a></li> 
        <li><a href="admin_adv_msg.php">Advertisement Manager</a></li> 
        <?php } ?> 
         <?php $chk = is_admin($_SESSION['l_type']) ;
            if($chk != 1) {
            ?>      
          <li><a href="eg_subs_msg.php">Quick Message</a></li>  
          <li><a href="#">Push Message</a></li>  
          <li><a href="eg_view_details.php">View Details</a></li>
          <li><a href="eg_gems.php">Gems</a></li>
          <li><a href="eg_offer_msg_list.php">Offer</a></li>
          <li><a href="eg_verify.php">Offer Verify</a></li>
           <li><a href="eg_verify_details.php">Offer Verify List</a></li>
          <?php } ?>
         
           <?php $chk = is_admin($_SESSION['l_type']) ;
            if($chk == 1) {
            ?>
          <li><a href="eg_selc_gems.php">Add Gems</a></li>
          <?php } ?>       
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>