<?php 
session_start();
require_once("../config/config.inc.php");
require_once("../config/functions.inc.php");
validate_admin();

$arr =$_POST['ids'];
$Submit =$_POST['Submit'];
if(count($arr)>0){
	$str_rest_refs=implode(",",$arr);
	if($Submit=='Delete')
	{
		$sql="delete from tbl_seo where id in ($str_rest_refs)"; 
		executeUpdate($sql);
		$sess_msg="Selected Record(s) Deleted Successfully";
		$_SESSION['sess_msg']=$sess_msg;
    }
	elseif($Submit=='Active')
	{
		$sql="update tbl_seo set status=1 where id in ($str_rest_refs)";
		executeUpdate($sql);
		$sess_msg="Selected Record(s) Actived Successfully";
		$_SESSION['sess_msg']=$sess_msg;
	}
	elseif($Submit=='Inactive')
	{
		$sql="update tbl_seo set status=0 where id in ($str_rest_refs)"; 
		executeUpdate($sql);
		$sess_msg="Selected Record(s) Inactived Successfully";
		$_SESSION['sess_msg']=$sess_msg;
	}	
}
else{
	$sess_msg="Please select Check Box";
	$_SESSION['sess_msg']=$sess_msg;
	header("Location: ".$_SERVER['HTTP_REFERER']);
	exit();
}

header("Location: seo_list.php?parentid=".$_REQUEST['parentid']);
exit();
?>