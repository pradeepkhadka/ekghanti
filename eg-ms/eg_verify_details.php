<?php
session_start(); 
require_once("config/config.inc.php");
require_once("config/functions.inc.php");
	validate_admin();
	@extract($_POST);
    $prg_id=$_SESSION['sess_prg_id'];  
    if (isset($prg_id))
    {
            $sql="SELECT * FROM tbl_offer_verify where status=0 order by datetime";  
	        $result=executeQuery($sql);
	        $num=mysql_num_rows($result);
	        
	      
    }


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php SITE_ADMIN_TITLE ?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
		<style type="text/css">
		div.dataTables_wrapper 
		div.dataTables_filter input {
		width: auto !important;
		}

		#datatb_wrapper {
			overflow: hidden !important;
		}
		</style>
 
		<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#datatb').DataTable({
					//"order": [[ 3, "desc" ]],
					"columnDefs": [ {
					"targets": [ 0, 2 ],
					"orderable": false
					} ],
					 "pageLength": 50
					});
			} );

			function verify(frmobj,comb,user_id)
				{
					if(comb=='verify'){
						if(confirm ("Are you sure you want to verify")){
							frmobj.action = "auth/add_eg_verify_offer.php";
							frmobj.submit();
							//alert(comb);
						}
						//else{ 
						//	return false;
						//}
					}
			
					
				}
		</script>
</head>
<body>
<?php include("header.inc.php");?>
<div class="container"> 		
	<!-- Center Part Begins Here  -->
	<div class="vv-center">
		<div class="title-info">
		<p align="center" class="warning"><?php echo (isset($_SESSION['sess_msg']) ? $_SESSION['sess_msg'] : ''); ?></p>	
		<h2>Offer Verify list</h2>
	</div>
		<form method="POST">
			<table class="table table-striped table-inverse" id="datatba">  
				<thead> 
					<tr> 
						<th>S.No</th> 
						<th>Number</th> 
						<th>Verify Code</th> 
						<th>Date</th> 
						<th>Action</th>
					</tr> 
				</thead> 
				<tbody> 
					<?php
					$i =1;					
					while($rc = mysql_fetch_array($result)) { ?>
					<tr> 
						<input type="hidden" name="id" value="<?php echo $rc['id']; ?>" />
						<td><?php echo $i; ?></td> 
						<td><?php echo $rc['phone_number']; ?></td> 
						<td><?php echo $rc['offer_code']; ?></td> 
						<td><?php echo $rc['datetime']; ?></td> 
						<td><button type="button" class="btn btn-primary" value="verified">Verified</button></td>
					</tr>
					<?php $i++; } ?>
				
				</tbody>
		 </table>

		
		</form>
	</div> 				
</div>	 
<?php include("footer.inc.php");?>
</body>
</html>
<?php
$_SESSION['sess_msg'] = '';
?>
