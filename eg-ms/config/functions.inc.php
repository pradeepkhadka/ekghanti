<?php
/******************************************************************************
*		File : functions.inc.php                                              *
*       Created By : Pradeep Khadka                                    *
*       Date Created : Tuesday 27 jan 2011, 1:05 PM                         *
*       Date Modified : Tuesday 27 jan 2011, 1:05 PM                        *
*       File Comment : This file contain functions which will use in coding.  *
*                                                                             *
*******************************************************************************/

// For Executing Query. This function returns a argument which contain recordset 
// object through it user can retrieve values of table.
function executeQuery($sql)
{
	$result = mysql_query($sql) or die("<span style='FONT-SIZE:11px; FONT-COLOR: #000000; font-family=tahoma;'><center>An Internal Error has Occured. Please report following error to the webmaster.<br><br>".$sql."<br><br>".mysql_error()."'</center></FONT>");
	return $result;
} 

	
      
function cust_send_mail($email_to,$emailto_name,$email_subject,$email_body,$email_from,$reply_to,$html=true)
{ 
	require_once "class.phpmailer.php";
	global $SITE_NAME;
	$mail = new PHPMailer();
	$mail->IsSMTP(); // send via SMTP
	$mail->Mailer   = "mail"; // SMTP servers
	$mail->From     = $email_from;
	$mail->FromName = $SITE_NAME;
	$mail->AddAddress($email_to,$emailto_name); 
	$mail->AddReplyTo($reply_to,$SITE_NAME);
	$mail->WordWrap = 50;                              // set word wrap
	$mail->IsHTML($html);                               // send as HTML
	$mail->Subject  =  $email_subject;
	$mail->Body     =  $email_body;
	$mail->Send();	
	return true;	
}

function cust_send_mail_with_attachment($email_to,$emailto_name,$email_subject,$email_body,$email_from,$reply_to,$html=true,$file_name)
{ 
	require_once "class.phpmailer.php";
	global $SITE_NAME;
	
	$mail = new PHPMailer();
	$mail->IsSMTP(); // send via SMTP
	$mail->Mailer   = "mail"; // SMTP servers
	
	$mail->From     = $email_from;
	$mail->FromName = $SITE_NAME;
	$mail->AddAddress($email_to,$emailto_name); 
	$mail->AddReplyTo($reply_to,$SITE_NAME);
	$mail->WordWrap = 50;                              // set word wrap
	$mail->IsHTML($html);                               // send as HTML
	$mail->Subject  =  $email_subject;
	$mail->Body     =  $email_body;
	
	for($i = 0; $i < count($file_name); $i++)
	{	
		 $mail->AddAttachment($file_name[$i]);
	}
	$mail->Send();	
	return true;	
}



function getSingleResult($sql)
{
	$response = "";
	$result = mysql_query($sql) or die("<center>An Internal Error has Occured. Please report following error to the webmaster.<br><br>".$sql."<br><br>".mysql_error()."'</center>");
	if ($line = mysql_fetch_array($result)) {
		$response = $line['0'];
	} 
	return $response;
} 

// For Executing Query. This function update the table by desired data.
function executeUpdate($sql)
{
	mysql_query($sql) or die("<center>An Internal Error has Occured. Please report following error to the webmaster.<br><br>".$sql."<br><br>".mysql_error()."'</center>");
}

// It returns the path of current file.
function getCurrentPath()
{
	global $_SERVER;
	return "http://" . $_SERVER['HTTP_HOST'] . getFolder($_SERVER['PHP_SELF']);
}

// This function adjusts the decimal point of argumented parameter and return the adjusted value.
function adjustAfterDecimal($param)
{
	if(strpos($param,'.')== "")
	{
		$final_value=$param.".00";
		return  $final_value;
	}
	$after_decimal  = substr($param , strpos($param,'.')+1, strlen($param) );	
	$before_decimal = substr($param,0 ,  strpos($param,'.'));
	if(strlen($after_decimal)<2)
	{
		if(strlen($after_decimal)==1)
		{
			$final_value=$param."0";
		}
		if(strlen($after_decimal)==0)
		{
			$final_value.="$param.00";
		}
	}
	else
	{
		$trim_value = substr($after_decimal,0,2);
		$final_value.=$before_decimal.".".$trim_value;
	}
	return $final_value;
}	

// This funtion is used for validating the front side users that he is logged in or not.
function validate_user()
{
	if($_SESSION['sess_uid']=='')
	{
		ms_redirect("index.php?back=$_SERVER[REQUEST_URI]");
	}
}

// This funtion is used for validating the admin side users that he is logged in or not.
function validate_admin()
{
	if($_SESSION['sess_uid']=='')
	{
		ms_redirect("index.php?back=$_SERVER[REQUEST_URI]");
	}
}
// This funtion is used for validating the frount side users (either expert or requester) that he is logged in or not.
function validate_front()
{
	if($_SESSION['f_uid']=='')
	{
		ms_redirect("index.php?back=$_SERVER[REQUEST_URI]");
	}
}
// This function is used for redirecting the file on desired file.
function ms_redirect($file, $exit=true, $sess_msg='')
{
	header("Location: $file");
	exit();
	
}

function get_qry_str($over_write_key = array(), $over_write_value= array())
{
	global $_GET;
	$m = $_GET;
	if(is_array($over_write_key)){
		$i=0;
		foreach($over_write_key as $key){
			$m[$key] = $over_write_value[$i];
			$i++;
		}
	}else{
		$m[$over_write_key] = $over_write_value;
	}
	$qry_str = qry_str($m);
	return $qry_str;
} 

function qry_str($arr, $skip = '')
{
	$s = "?";
	$i = 0;
	foreach($arr as $key => $value) {
		if ($key != $skip) {
			if(is_array($value)){
				foreach($value as $value2){
					if ($i == 0) {
						$s .= "$key%5B%5D=$value2";
					$i = 1;
					} else {
						$s .= "&$key%5B%5D=$value2";
					} 
				}		
			}else{
				if ($i == 0) {
					$s .= "$key=$value";
					$i = 1;
				} else {
					$s .= "&$key=$value";
				} 
			}
		} 
	} 
	return $s;
} 


function ms_print_r($var)
{
	global $local_mode;
	if ($local_mode || $debug) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
	}
} 

function add_slashes($param)
{
	$k_param = addslashes(stripslashes($param));
	return $k_param;
} 

function ms_stripslashes($text)
{
	if (is_array($text)) {
		$tmp_array = Array();
		foreach($text as $key => $value) {
			$tmp_array[$key] = ms_stripslashes($value);
			} 
		return $tmp_array;
	} else {
		return stripslashes($text);
	} 
} 

function ms_addslashes($text)
{
	if (is_array($text)) {
		$tmp_array = Array();
		foreach($text as $key => $value) {
			$tmp_array[$key] = ms_addslashes($value);
		} 
		return $tmp_array;
	} else {
		return addslashes(stripslashes($text));
	} 
} 

function html2text($html)
{
	$search = array ("'<head[^>]*?>.*?</head>'si", // Strip out javascript
		"'<script[^>]*?>.*?</script>'si", // Strip out javascript
		"'<[\/\!]*?[^<>]*?>'si", // Strip out html tags
		"'([\r\n])[\s]+'", // Strip out white space
		"'&(quot|#34);'i", // Replace html entities
		"'&(amp|#38);'i",
		"'&(lt|#60);'i",
		"'&(gt|#62);'i",
		"'&(nbsp|#160);'i",
		"'&(iexcl|#161);'i",
		"'&(cent|#162);'i",
		"'&(pound|#163);'i",
		"'&(copy|#169);'i",
		"'&#(\d+);'e"); // evaluate as php
	$replace = array ("",
		"",
		"",
		"\\1",
		"\"",
		"&",
		"<",
		">",
		" ",
		chr(161),
		chr(162),
		chr(163),
		chr(169),
		"chr(\\1)");
	$text = preg_replace ($search, $replace, $html); 
	return $text;
} 

function sort_arrows($column){
	global $_SERVER;
	return '<A HREF="'.$_SERVER['PHP_SELF'].get_qry_str(array('order_by','order_by2'), array($column,'asc')).'"><IMG SRC="images/white_up.gif" BORDER="0"></A> <A HREF="'.$_SERVER['PHP_SELF'].get_qry_str(array('order_by','order_by2'), array($column,'desc')).'"><IMG SRC="images/white_down.gif" BORDER="0"></A>';
}

function unlink_file( $file_name , $folder_name )
{
	$file_path = $folder_name."/".$file_name;
	@chmod ($foleder_name , 0777);
	@unlink($file_path);
	return true;	
}
function getUserName($id) {
	$query = "select * from user where userid=$id";
	$rs = executeQuery($query);
	$row = mysql_fetch_array($rs);
	return $row["username"];
}
function getUserEmail($id) {
	$query = "select * from user where userid=$id";
	$rs = executeQuery($query);
	$row = mysql_fetch_array($rs);
	return $row["email"];
}

function showImage($imageName) {
	if(is_file(SITE_FS_PATH."/property_pic/".$imageName)) {
		$rtVar = "<img src='property_pic/".$imageName."' width='107'>";
	} else {
		$rtVar ='<img src="images/no-image.jpg" width="107" height="105">';
	}
	return	$rtVar;
}
function getPropertyType($id) {
	$query = "select * from property_type where id=".$id;
	$rs = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_array($rs);
	return $row["title"];
}
function getName($id) {
	$query = "select * from user where userid=".$id;
	$rs = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_array($rs);
	return $row["fname"]." ".$row["lname"];
}
function getContentMenu($id) {
	$query = "select title from tbl_content where id=".$id;
	$rs = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_array($rs);
	return $row["title"];
}
function job($id) {
	
	return $id;
}
function getEmail($id) {
	$query = "select * from user where userid=".$id;
	$rs = mysql_query($query) or die(mysql_error());
	$row = mysql_fetch_array($rs);
	return $row["email"];
}
function dateformat($datee){
$datestr=explode("-",$datee);
$datrarr['01']="Jan";
$datrarr['02']="Feb";
$datrarr['03']="March";
$datrarr['04']="April";
$datrarr['05']="May";
$datrarr['06']="June";
$datrarr['07']="July";
$datrarr['08']="Augest";
$datrarr['09']="Sept";
$datrarr['10']="Oct";
$datrarr['11']="Nov";
$datrarr['12']="Dec";
return $datrarr[$datestr[1]]." ".$datestr[2].", ".$datestr[0];
}
function ConvertDate($d)
	{
		 $new_a=split("[ ,]",$dac);
		$d=$new_a[1];
		$mm=$new_a[0];
		$y=$new_a[3];
		if($mm=='Jan'){$m=1;}
		else if($mm=='Jan'){$m=1;}
		else if($mm=='Feb'){$m=2;}
		else if($mm=='Mar'){$m=3;}
		else if($mm=='Apr'){$m=4;}
		else if($mm=='May'){$m=5;}
		else if($mm=='Jun'){$m=6;}
		else if($mm=='Jul'){$m=7;}
		else if($mm=='Aug'){$m=8;}
		else if($mm=='Sep'){$m=9;}
		else if($mm=='Oct'){$m=10;}
		else if($mm=='Nov'){$m=11;}
		else if($mm=='Dec'){$m=12;}
		
		$query_date=date("Y-m-d",mktime(0,0,0,$m,$d,$y));
		return $query_date;
	} 

	function getProductFields($type,$field,$id)
	{
		switch ($type) {
			case 1:
			    	$query = "select ".  $field." from tbl_battery where product_id=".$id;
					$rs = mysql_query($query); 
					$row = mysql_fetch_array($rs); //echo $id.'/'.$field.'/'.$query ; //exit();
					return $row[$field];
				
				break;
			case 2:
			        $query = "select ".  $field." from tbl_panel where product_id=".$id;
					$rs = mysql_query($query); 
					$row = mysql_fetch_array($rs); //echo $id.'/'.$field.'/'.$query ; //exit();
					return $row[$field];
			break;
			case 3:
			        $query = "select ".  $field." from tbl_inverter where product_id=".$id;
					$rs = mysql_query($query); 
					$row = mysql_fetch_array($rs); //echo $id.'/'.$field.'/'.$query ; //exit();
					return $row[$field];
			break;
			case 4:
			        $query = "select ".  $field." from tbl_charge where product_id=".$id;
					$rs = mysql_query($query); 
					$row = mysql_fetch_array($rs); //echo $id.'/'.$field.'/'.$query ; //exit();
					return $row[$field];
			break;
			
			default:
				# code...
				break;
		}
	} 

	function getAllMonth($month,$year)
	{
		$mont= $month;
		$yr= $year;
		$conv= $year * 12;
		return $month + $conv; 
	}

    function getPrgGems($prg_id)
	{
		$query = "SELECT *  FROM tbl_gems_details as tgd WHERE tgd.prg_id = ".$prg_id ." order by tgd.date desc";
		$rs = executeQuery($query);
		//$row = mysql_fetch_array($rs);
		return $rs;
	}

	function is_admin($type)
		{
		if($type==202) {
			return true;
		}else{
		return false;
		}
	}

	function get_usrByprg($prg_id)
		{
		$query = "SELECT prg_user from tbl_prg where prg_id=" .$prg_id;
		$rs = executeQuery($query);
		$row = mysql_fetch_array($rs);
		return $row['prg_user'];	
		
		}

	
?>