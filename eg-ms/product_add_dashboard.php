<?php 
session_start();
require_once("../config/config.inc.php");
require_once("../config/functions.inc.php");
validate_admin();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php SITE_ADMIN_TITLE?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body >
<?php include("header.inc.php");?>
	<div class="container">
		<!-- Center Part Begins Here  -->
	<div class="abc">
			<!-- <h1>Please select what you want?</h1> -->
		

		<div class="p-btn-holder">
			<a href="product_add_panel.php"> <img src="images/panel.png"> panel</a> 
			<a href="product_add_battery.php"><img src="images/battery.png"> Battery</a> 
			<a href="product_add_charge.php"><img src="images/controller.png"> Charge controller</a>
			<a href="product_add_inverter.php"><img class="inv" src="images/inverter.png"> Inverter / Charger</a>	
		</div>
		<!-- <div> -->

		</div>
	<!-- Center Part Ends Here  -->
	</div>


	
	
<?php include("footer.inc.php");?>

</body>
</html>

