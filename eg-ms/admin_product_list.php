<?php 
session_start();
require_once("../config/config.inc.php");
require_once("../config/functions.inc.php");

@extract($_POST);
validate_admin();
$user_id=$_SESSION['sess_uid'];
$start=0;
$pp=20;
$pagesize=$pp;

if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];

$order_by="id";

$column="select * ";
$sql=" from tbl_product";
$sql1="select count(*) ".$sql;
$sql=$column.$sql;
$sql.=" order by id desc  limit $start, $pagesize ";
//echo "<br>$sql</br>";
$result=executequery($sql);
$resultr=executequery($sql);
$reccnt=getSingleResult($sql1);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=SITE_ADMIN_TITLE?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script

<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/dt/dt-1.10.10/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/s/dt/dt-1.10.10/datatables.min.js"></script>



<script>
$(document).ready(function() {
    $('#example').DataTable({
        "order": [[ 0, "asc" ]]
    });
} );

function checkall(objForm)
{
	len = objorm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++){
		if (objForm.elements[i].type=='checkbox') 
			objForm.elements[i].checked=objForm.check_all.checked;
	}
}

function del_prompt(frmobj,comb)
{
	if(comb=='Delete'){
		if(confirm ("Are you sure you want to delete Record(s)")){
			frmobj.action = "seo_del.php";
			frmobj.submit();
		}
		else{ 
			return false;
		}
	}
	else
	{
		frmobj.action = "seo_del.php";
		frmobj.submit();
	}
	
}
function validate_form(obj)
{alert(obj);
	var msg='Incomplete Data, Kindly fill required fields...\n\n', flag=false;
	
	if(obj.displayName.value == '') msg+='- Please Enter Display Name. \n';
	if(msg == 'Incomplete Data, Kindly fill required fields...\n\n')
		return true;
	else{
		alert(msg);
		return false;
	}
	
}
function change_records(val)
{
	location.href='seo_list.php?p='+val;
}

  var tableToExcel = (function() { 
    var uri = 'data:application/vnd.ms-excel;base64,',
      template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
      base64 = function(s) {
        return window.btoa(unescape(encodeURIComponent(s)))
      },
      format = function(s, c) {
        return s.replace(/{(\w+)}/g, function(m, p) {
          return c[p];
        })
      }
      return function(table, name, filename) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {
          worksheet: name || 'Worksheet',
          table: table.innerHTML
        }

        document.getElementById("dlink").href = uri + base64(format(template, ctx));
        document.getElementById("dlink").download = filename;
        document.getElementById("dlink").click();

      }
  })()

</script></head>
<body>
<?php include("header.inc.php");?>
<div class="container footer-height">

  <h2>Product Listing</h2>
  <a id="dlink" style="display:none;"></a>
 <span class="excel">
  <img style="cursor: pointer"
  src="images/excel.png"
  id="btnExport"
  onclick="tableToExcel('dvData', 'Report','aa.xls' )" /></span>
            
  <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>Product Type</th>
        <th>Product Name</th>
        <th>Model</th>
        <th>Price</th>
        <th>Brand</th>
        <th>Quantity</th>
        <th>Warranty</th>
        <th>Available</th>
        <th>NEPQA Compliant</th>
      </tr>
    </thead>
    <tbody>
    <?php while($line=mysql_fetch_array($result)) { ?>
 <tr>
        <td><?php echo $line['product_type']; ?></td>
        <td><?php echo $line['product_name']; ?></td>
        <td><?php echo getProductFields($line['product_type_id'],'model',$line['id']) ?></td>
        <td><?php echo $line['price']; ?></td>
        <td><?php echo getProductFields($line['product_type_id'],'brand',$line['id']) ?></td>
        <td><?php echo $line['quantity']; ?></td>
        <td><?php echo getAllMonth(getProductFields($line['product_type_id'],'wrr_month',$line['id']),getProductFields($line['product_type_id'],'wrr_year',$line['id'])) ?></td>
        <td><?php echo $line['available_date']; ?></td>
        <td><?php $chek = getProductFields($line['product_type_id'],'gen_nepqa',$line['id']);?>

         <input type="checkbox" name="certificate_chk" value="y" <?php if (isset($chek)) { echo  ($chek == 'y' ? 'checked=checked' : ''); } ?>>
    </td>
      </tr>
      <?php } ?>    

    </tbody>
  </table>
<div id="dvData" style="display:none">
<table width="100%">
    <thead>
      <tr>
        <th>Product Type</th>
        <th>Product Name</th>
        <th>Model</th>
        <th>Price</th>
        <th>Brand</th>
        <th>Quantity</th>
        <th>Warranty</th>
        <th>Available</th>
        <th>NEPQA Compliant</th>
      </tr>
    </thead>
    <tbody>
    <?php while($liner=mysql_fetch_array($resultr)) { ?>
 <tr>
        <td><?php echo $liner['product_type']; ?></td>
        <td><?php echo $liner['product_name']; ?></td>
        <td><?php echo getProductFields($liner['product_type_id'],'model',$liner['id']) ?></td>
        <td><?php echo $liner['price']; ?></td>
        <td><?php echo getProductFields($liner['product_type_id'],'brand',$liner['id']) ?></td>
        <td><?php echo $liner['quantity']; ?></td>
         <td><?php echo getAllMonth(getProductFields($liner['product_type_id'],'wrr_month',$liner['id']),getProductFields($liner['product_type_id'],'wrr_year',$liner['id'])) ?></td>
        <td><?php echo $liner['available_date']; ?></td>
        <td><?php $chek = getProductFields($liner['product_type_id'],'gen_nepqa',$liner['id']);?>

         <input type="checkbox" name="certificate_chk" value="y" <?php if (isset($chek)) { echo  ($chek == 'y' ? 'checked=checked' : ''); } ?>>
    </td>
      </tr>
      <?php } ?>    

    </tbody>
  </table>

</div> 


</div>
<?php include("footer.inc.php"); ?>
</body>
</html>
