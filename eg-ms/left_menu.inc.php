<?
session_start(); 
require_once("../config/config.inc.php");
require_once("../config/functions.inc.php");
validate_admin();
$user_type = $_SESSION['type'];
?>

<div class="left_menu" width="200px">
<ul>
	<li><a href="admin_home.php" class="menuLinks"><strong>Home<?php echo  $_SESSION['type']; ?></strong></a></li>
    <?php if($_SESSION['type']==2) {  ?>
    <li><a href="user_add.php" class="menuLinks"><strong>Create User</strong></a></li> 
    <?php } ?>
    <?php if($_SESSION['type'] == 1) { ?>
    <li><a href="product_list.php" class="menuLinks"><strong>Product Manager</strong></a></li> 
    <?php } ?>	
    <li><a href="admin_pref.php" class="menuLinks"><strong>Change Password</strong></a></li>
    <li><a href="logout.php" class="menuLinks"><strong>Logout</strong></a></li>
</ul>	
</div>
