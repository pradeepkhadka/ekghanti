<?php
session_start(); 
require_once("config/config.inc.php");
require_once("config/functions.inc.php");
	validate_admin();
	@extract($_POST);
    $prg_id=$_SESSION['sess_prg_id'];   

    if (isset($prg_id))
    {
            $sql="SELECT * FROM  tbl_reg_number_details tg where tg.trn_prg_id=".$prg_id." order by trn_id desc limit 0, 1000 "; 
	        $result=executeQuery($sql);	     

    }


?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php SITE_ADMIN_TITLE ?></title>
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="shortcut icon" type="image/png" href="../img/fav.png"/>

		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
		<style type="text/css">
		div.dataTables_wrapper 
		div.dataTables_filter input {
		width: auto !important;
		}

		#datatb_wrapper {
			overflow: hidden !important;
		}
		</style>
 
		<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#datatb').DataTable({
					//"order": [[ 3, "desc" ]],
					"columnDefs": [ {
					"targets": [ 0, 2 ],
					"orderable": false
					} ],
					 "pageLength": 50
					});
			} );
		</script>
</head>
<body>
<?php include("header.inc.php");?>
<div class="container"> 		
	<!-- Center Part Begins Here  -->
	<div class="vv-center">
		<div class="title-info">
		<h2>Message Details</h2>
		
	</div>
		
		<div class='form-wrapper'>
			
			 <div class="table-responsive">          
			  <table class="table" id="datatb">
			    <thead>
			      <tr>
			        <th>S.No</th>
			        <th>User No.</th>			        
			        <th>Program No.</th>
			        <th>Date/Time</th>       
			      </tr>
			    </thead>
			    <tbody>
				<?php
					$i =1;					
					while($rc = mysql_fetch_array($result)) { ?>
					<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $rc['trn_usr_number']; ?></td>
					<td><?php echo $rc['trn_prg_number']; ?></td>					
					<td><?php echo $rc['trn_miscall_date'].'/'.$rc['trn_miscall_time']; ?> </td>
					</tr>
				<?php $i++; } ?>
			    </tbody>
			  </table>
			  </div>

		</div>
		
	</div> 				
</div>

<?php include("footer.inc.php");?>

</body>
</html>
<?php
$_SESSION['sess_msg'] = '';
?>
