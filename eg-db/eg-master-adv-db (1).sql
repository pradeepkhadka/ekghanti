-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2016 at 07:07 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `eg-master-adv-db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_adv_msg`
--

CREATE TABLE IF NOT EXISTS `tbl_adv_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prg_id` int(11) NOT NULL,
  `msg` text NOT NULL,
  `prg_number` varchar(50) NOT NULL,
  `status` int(5) NOT NULL,
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_adv_msg`
--

INSERT INTO `tbl_adv_msg` (`id`, `prg_id`, `msg`, `prg_number`, `status`, `add_time`, `update_time`) VALUES
(1, 1, 'sdsdsd', '9801223322', 1, '2016-07-02 09:05:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gems`
--

CREATE TABLE IF NOT EXISTS `tbl_gems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `prg_id` int(11) NOT NULL,
  `gems` int(11) DEFAULT NULL,
  `gms_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gms_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_gems`
--

INSERT INTO `tbl_gems` (`id`, `user_id`, `prg_id`, `gems`, `gms_date`, `gms_update`) VALUES
(2, 3, 1, 917, '2016-08-21 09:09:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gems_details`
--

CREATE TABLE IF NOT EXISTS `tbl_gems_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `prg_id` int(11) DEFAULT NULL,
  `value` int(11) NOT NULL,
  `gems_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_gems_details`
--

INSERT INTO `tbl_gems_details` (`id`, `user_id`, `prg_id`, `value`, `gems_id`, `date`, `type`) VALUES
(4, NULL, 1, 1000, NULL, '2016-05-26 14:50:18', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_msg_map`
--

CREATE TABLE IF NOT EXISTS `tbl_msg_map` (
  `tmm_id` int(100) NOT NULL AUTO_INCREMENT,
  `tmm_prg_id` int(10) NOT NULL,
  `tmm_date` date NOT NULL,
  `tmm_time` time NOT NULL,
  `day` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`tmm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_msg_map`
--

INSERT INTO `tbl_msg_map` (`tmm_id`, `tmm_prg_id`, `tmm_date`, `tmm_time`, `day`, `status`) VALUES
(1, 1, '2016-01-17', '23:38:46', 'Sunday', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_msg_setup`
--

CREATE TABLE IF NOT EXISTS `tbl_msg_setup` (
  `tms_id` int(100) NOT NULL AUTO_INCREMENT,
  `tms_prg_id` int(10) NOT NULL,
  `count` int(10) DEFAULT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`tms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offer_msg`
--

CREATE TABLE IF NOT EXISTS `tbl_offer_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prg_id` int(11) NOT NULL,
  `msg` text NOT NULL,
  `status` int(11) NOT NULL,
  `ufd` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_offer_msg`
--

INSERT INTO `tbl_offer_msg` (`id`, `prg_id`, `msg`, `status`, `ufd`) VALUES
(1, 1, 'Thankyou for Checking in more than 3 times. Ask free Frappie with Redmud Helpers around you.\r\n', 1, 'General offer');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offer_range`
--

CREATE TABLE IF NOT EXISTS `tbl_offer_range` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prg_id` int(11) NOT NULL,
  `msg_id` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `day_to` varchar(255) NOT NULL,
  `day_from` varchar(255) NOT NULL,
  `status` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_offer_range`
--

INSERT INTO `tbl_offer_range` (`id`, `prg_id`, `msg_id`, `days`, `day_to`, `day_from`, `status`) VALUES
(11, 1, 1, 2, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offer_verify`
--

CREATE TABLE IF NOT EXISTS `tbl_offer_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_code` varchar(100) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `prg_id` int(11) NOT NULL,
  `datetime` varchar(100) NOT NULL,
  `status` int(10) NOT NULL,
  `udf` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prg`
--

CREATE TABLE IF NOT EXISTS `tbl_prg` (
  `prg_id` int(10) NOT NULL AUTO_INCREMENT,
  `prg_code` varchar(50) DEFAULT NULL,
  `prg_name` varchar(100) NOT NULL,
  `prg_msg` varchar(250) NOT NULL,
  `prg_subc_msg` text,
  `prg_user` int(10) NOT NULL,
  `prg_type` varchar(50) NOT NULL,
  `prg_number` varchar(50) DEFAULT NULL,
  `prg_cat` int(100) NOT NULL,
  `prg_start_date` date NOT NULL,
  `prg_end_date` date NOT NULL,
  `prg_start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `prg_end_time` time NOT NULL,
  `prg_no_status` int(11) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`prg_id`),
  KEY `prg_user` (`prg_user`,`prg_cat`),
  KEY `prg_cat` (`prg_cat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_prg`
--

INSERT INTO `tbl_prg` (`prg_id`, `prg_code`, `prg_name`, `prg_msg`, `prg_subc_msg`, `prg_user`, `prg_type`, `prg_number`, `prg_cat`, `prg_start_date`, `prg_end_date`, `prg_start_time`, `prg_end_time`, `prg_no_status`, `status`) VALUES
(1, NULL, 'eg-redmud', '', 'PASSWORD:redmudpass\r\nTODAY''S SPECIAL\r\nCold coffee @200\r\nIce mock coffee @250\r\n\r\n', 3, 'quickcall', '9801223322', 3, '0000-00-00', '0000-00-00', '2016-07-02 09:04:48', '00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prg_cat`
--

CREATE TABLE IF NOT EXISTS `tbl_prg_cat` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tpc_name` varchar(200) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_prg_cat`
--

INSERT INTO `tbl_prg_cat` (`id`, `tpc_name`, `status`) VALUES
(1, 'Enterprise News', 1),
(2, 'Political Show, NTV, Series', 1),
(3, 'resturant', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prg_vote`
--

CREATE TABLE IF NOT EXISTS `tbl_prg_vote` (
  `prg_vote_id` int(10) NOT NULL AUTO_INCREMENT,
  `prg_vote_prg_id` int(10) NOT NULL,
  `prg_vote_title` int(10) DEFAULT NULL,
  `prg_vote_desc` text NOT NULL,
  `status` int(5) NOT NULL,
  `udf` int(5) DEFAULT NULL,
  PRIMARY KEY (`prg_vote_id`),
  KEY `prg_vote_prg_id` (`prg_vote_prg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prg_vote_number`
--

CREATE TABLE IF NOT EXISTS `tbl_prg_vote_number` (
  `tpvn_id` int(10) NOT NULL AUTO_INCREMENT,
  `tpvn_prg_id` int(10) DEFAULT NULL,
  `tpvn_phone_number` varchar(50) NOT NULL,
  `tpvn_type` varchar(100) NOT NULL,
  `tpvn_tbl_prg_vote_id` int(10) NOT NULL,
  `tpvn_usr_number` varchar(50) NOT NULL,
  `count` int(10) NOT NULL,
  `prg_vote_status` int(10) NOT NULL,
  PRIMARY KEY (`tpvn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reg_checkin_details`
--

CREATE TABLE IF NOT EXISTS `tbl_reg_checkin_details` (
  `trn_id` int(100) NOT NULL AUTO_INCREMENT,
  `check_code` varchar(255) DEFAULT NULL,
  `trn_usr_number` varchar(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `trn_prg_number` varchar(50) NOT NULL,
  `trn_miscall_date` date NOT NULL,
  `trn_miscall_time` time NOT NULL,
  `trn_prg_id` int(11) NOT NULL,
  `trn_callcount` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`trn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_reg_checkin_details`
--

INSERT INTO `tbl_reg_checkin_details` (`trn_id`, `check_code`, `trn_usr_number`, `name`, `trn_prg_number`, `trn_miscall_date`, `trn_miscall_time`, `trn_prg_id`, `trn_callcount`, `status`) VALUES
(3, '1304917101', '9816448812', NULL, '9801223322', '2016-01-20', '11:54:10', 1, 1, 1),
(4, '413833864', '9816448812', NULL, '9801223322', '2016-01-21', '11:54:10', 1, 4, 1),
(5, '1400279508', '9816448812', NULL, '9801223322', '2016-01-22', '11:54:10', 1, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reg_checkin_details_arch`
--

CREATE TABLE IF NOT EXISTS `tbl_reg_checkin_details_arch` (
  `trn_id` int(100) NOT NULL AUTO_INCREMENT,
  `check_code` varchar(255) DEFAULT NULL,
  `trn_usr_number` varchar(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `trn_prg_number` varchar(50) NOT NULL,
  `trn_miscall_date` date NOT NULL,
  `trn_miscall_time` time NOT NULL,
  `trn_prg_id` int(11) NOT NULL,
  `trn_callcount` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`trn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reg_number`
--

CREATE TABLE IF NOT EXISTS `tbl_reg_number` (
  `trn_id` int(100) NOT NULL AUTO_INCREMENT,
  `trn_usr_number` varchar(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `trn_prg_number` varchar(50) NOT NULL,
  `trn_miscall_date` date NOT NULL,
  `trn_miscall_time` time NOT NULL,
  `trn_prg_id` int(11) NOT NULL,
  `trn_callcount` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`trn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_reg_number`
--

INSERT INTO `tbl_reg_number` (`trn_id`, `trn_usr_number`, `name`, `trn_prg_number`, `trn_miscall_date`, `trn_miscall_time`, `trn_prg_id`, `trn_callcount`, `status`) VALUES
(14, '9816448812', NULL, '9801223322', '2016-01-22', '11:54:10', 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reg_number_details`
--

CREATE TABLE IF NOT EXISTS `tbl_reg_number_details` (
  `trn_id` int(100) NOT NULL AUTO_INCREMENT,
  `check_code` varchar(255) DEFAULT NULL,
  `trn_usr_number` varchar(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `trn_prg_number` varchar(50) NOT NULL,
  `trn_miscall_date` date NOT NULL,
  `trn_miscall_time` time NOT NULL,
  `trn_prg_id` int(11) NOT NULL,
  `trn_callcount` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`trn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

--
-- Dumping data for table `tbl_reg_number_details`
--

INSERT INTO `tbl_reg_number_details` (`trn_id`, `check_code`, `trn_usr_number`, `name`, `trn_prg_number`, `trn_miscall_date`, `trn_miscall_time`, `trn_prg_id`, `trn_callcount`, `status`) VALUES
(113, NULL, '9816448812', NULL, '9801223322', '2016-01-20', '11:54:10', 1, 1, 1),
(114, NULL, '9816448812', NULL, '9801223322', '2016-01-20', '11:54:10', 1, 1, 2),
(115, NULL, '9816448812', NULL, '9801223322', '2016-01-20', '11:54:10', 1, 1, 3),
(116, NULL, '9816448812', NULL, '9801223322', '2016-01-21', '11:54:10', 1, 1, 4),
(117, NULL, '9816448812', NULL, '9801223322', '2016-01-22', '11:54:10', 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_send_msg`
--

CREATE TABLE IF NOT EXISTS `tbl_send_msg` (
  `tsm_id` int(100) NOT NULL AUTO_INCREMENT,
  `tsm_usr_number` varchar(50) NOT NULL,
  `tsm_msg` varchar(250) NOT NULL,
  `tsm_prg_id` int(11) NOT NULL,
  `tsm_status` varchar(100) NOT NULL,
  `tsm_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tsm_id`),
  KEY `tsm_prg_id` (`tsm_prg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sim_number`
--

CREATE TABLE IF NOT EXISTS `tbl_sim_number` (
  `tsn_id` int(10) NOT NULL AUTO_INCREMENT,
  `tsn_number` varchar(50) NOT NULL,
  `tsn_prg_id` int(20) DEFAULT NULL,
  `counter` int(100) DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`tsn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_sim_number`
--

INSERT INTO `tbl_sim_number` (`tsn_id`, `tsn_number`, `tsn_prg_id`, `counter`, `status`) VALUES
(2, '9843921001', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `usr_id` int(10) NOT NULL AUTO_INCREMENT,
  `usr_name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `usr_email` varchar(200) DEFAULT NULL,
  `usr_phone` varchar(50) DEFAULT NULL,
  `usr_type` int(20) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`usr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`usr_id`, `usr_name`, `password`, `usr_email`, `usr_phone`, `usr_type`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'pradeepkhadka.ddn@gmail.com', '9816448812', 202, '1'),
(3, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'hello@vimvoxlab.com', '9801188406', 1, '1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_prg`
--
ALTER TABLE `tbl_prg`
  ADD CONSTRAINT `tbl_prg_ibfk_1` FOREIGN KEY (`prg_user`) REFERENCES `tbl_user` (`usr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_prg_ibfk_2` FOREIGN KEY (`prg_cat`) REFERENCES `tbl_prg_cat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_prg_vote`
--
ALTER TABLE `tbl_prg_vote`
  ADD CONSTRAINT `tbl_prg_vote_ibfk_1` FOREIGN KEY (`prg_vote_prg_id`) REFERENCES `tbl_prg` (`prg_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
