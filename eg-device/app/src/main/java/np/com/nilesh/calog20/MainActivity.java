package np.com.nilesh.calog20;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import np.com.nilesh.calog20.MyService;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    static String fquery ="";
    static String Urlstring;
    static String Outputval;
    static String phonenum1;
    static String phonenum2;
    static String Link1;
    static String Link2;
    static String webPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        simcheck();


        Button button_exit = (Button) findViewById(R.id.button_exit);
        button_exit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                finishActivity(0);
                System.exit(0);


            }

        });

        Button button_log = (Button) findViewById(R.id.button_log);
        button_log.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {


                simcheck();
                TextView textView = (TextView) findViewById(R.id.textView);
                textView.setText(MyService.Textquery);


            }

        });

        Button button_db = (Button) findViewById(R.id.button_db);
        button_db.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                String Urlstring = ("http://aayo.xyz/api/MissCallAayo/post.php?d=" + MainActivity.fquery);
                TextView textView = (TextView) findViewById(R.id.textView);

            }

        });

    }





    public void simcheck() {


        final SharedPreferences loginfo = getApplicationContext().getSharedPreferences("loginfo", MODE_PRIVATE);
        final SharedPreferences.Editor neweditor = loginfo.edit();


        if (loginfo.getBoolean("log_no", false) == true) {

            View view_button = findViewById(R.id.button_number);
            view_button.setVisibility(view_button.GONE);
            View text_num1 = findViewById(R.id.text_phnum1);
            text_num1.setVisibility(text_num1.GONE);
            View text_num2 = findViewById(R.id.text_phnum2);
            text_num2.setVisibility(text_num2.GONE);
            View text_link1 = findViewById(R.id.text_link1);
            text_link1.setVisibility(text_link1.GONE);
            View text_link2 = findViewById(R.id.text_link2);
            text_link2.setVisibility(text_link2.GONE);

            phonenum1 = loginfo.getString("Cell_Num1", "");
            phonenum2 = loginfo.getString("Cell_Num2","");
            Link1 =  loginfo.getString("Link1","");
            Link2 =  loginfo.getString("Link2","");
            TextView textView = (TextView) findViewById(R.id.textView);
            textView.append("\n Sim 1 : " +  phonenum1 +"\n Sim 1 : " + phonenum2 + "\n \n Sim1 Link : " + Link1 + "\n \n Sim2 Link : " + Link2);

            Intent intent = new Intent(getApplicationContext(), MyService.class);
            startService(intent);



        } else {
            final View view_button = findViewById(R.id.button_number);
            final View text_num1 = findViewById(R.id.text_phnum1);
            final View text_num2 = findViewById(R.id.text_phnum2);
            final View text_link1 = findViewById(R.id.text_link1);
            final View text_link2 = findViewById(R.id.text_link2);
            final TextView text_number1 = (TextView) findViewById(R.id.text_phnum1);
            final TextView text_number2 = (TextView) findViewById(R.id.text_phnum2);
            final TextView link1 = (TextView) findViewById(R.id.text_link1);
            final TextView link2 = (TextView) findViewById(R.id.text_link2);



            Button button = (Button) findViewById(R.id.button_number);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View V) {

                    neweditor.clear();
                    neweditor.putString("Cell_Num1", text_number1.getText().toString());
                    neweditor.putString("Cell_Num2", text_number2.getText().toString());
                    neweditor.putString("Link1", link1.getText().toString());
                    neweditor.putString("Link2", link2.getText().toString());

                    neweditor.putBoolean("log_no", true);
                    neweditor.commit();

                    view_button.setVisibility(view_button.GONE);
                    text_num1.setVisibility(text_num1.GONE);
                    text_num2.setVisibility(text_num2.GONE);
                    text_link1.setVisibility(text_link1.GONE);
                    text_link2.setVisibility(text_link2.GONE);

                }

            });

            phonenum1 = text_number1.getText().toString();
            phonenum2 = text_number2.getText().toString();
            Link1 = link1.getText().toString();
            Link2 = link2.getText().toString();

            Intent intent = new Intent(MainActivity.this, MyService.class);
            startService(intent);

        }

    }

}
