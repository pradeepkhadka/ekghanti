package np.com.nilesh.calog20;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by nilesh on 1/20/16.
 */
public class BootCompleteReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent bootintent = new Intent(context.getApplicationContext(), MyService.class);
        context.startService(bootintent);
    }
}
