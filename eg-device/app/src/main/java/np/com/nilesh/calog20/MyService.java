package np.com.nilesh.calog20;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.*;
import android.provider.CallLog;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import np.com.nilesh.calog20.MainActivity;


public class MyService extends Service {


    static String sim1query = "";
    static String sim2query = "";
    static String Textquery="";
    static String Outputval;
    final Handler handler = new Handler();
    static String webPage;
    static String data;
    static String phonenum1;
    static String phonenum2;
    static String Link1;
    static String Link2;
    static int linkcount1;
    static int linkcount2;
    static int tempid;
    static int i1;
    static int i2;
    static int simtype;
    static int statcount1 = 0;
    static int statcount2 = 0;
    static int limit1 = 0;
    static int limit2 = 0;
    static int templimit1 = 0;
    static int templimit2 = 0;
    String tempquery1;
    String tempquery2;
    static String sim1string;
    static String sim2string;
    static int logcount;
    static Boolean status1= false;
    static Boolean status2 = false;
    static int refreshrate;


    public MyService() {
    }





    public int onStartCommand(Intent intent, int flags, int startId) {

        IntentFilter filter = new IntentFilter();
        filter.addAction(android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        registerReceiver(miscallreceiver, filter);

        final SharedPreferences loginfo = getApplicationContext().getSharedPreferences("loginfo", MODE_PRIVATE);
        final SharedPreferences.Editor neweditor = loginfo.edit();

            PendingIntent pendingintent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
            Notification  notification = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle("Calog 2.0 is running")
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentIntent(pendingintent)
                    .setSmallIcon(R.drawable.klog)
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                            R.drawable.klog))
                    .setContentText("Click to open Calog").build();

            }


        startForeground(1234, notification);



        runnable.run();
        return START_STICKY;
    }



    public IBinder onBind(Intent intent) {
        return null;
    }


    public void create() {

        final SharedPreferences loginfo = getApplicationContext().getSharedPreferences("loginfo", MODE_PRIVATE);
        phonenum1 = loginfo.getString("Cell_Num1", "");
        phonenum2 = loginfo.getString("Cell_Num2", "");
        Link1 = loginfo.getString("Link1", "");
        Link2 = loginfo.getString("Link2", "");
        limit1 = 0;
        limit2 = 0;
        templimit1 = 0;
        templimit2 = 0;
        i1 = 1;
        i2 = 1;
        String strOrderASC = CallLog.Calls.DATE + " ASC";
        String strOrderDESC = CallLog.Calls.DATE + " DESC";

        ContentResolver resolver = getContentResolver();

        Cursor managedCursor = resolver.query(CallLog.Calls.CONTENT_URI, null,
                null, null, strOrderASC);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int count = managedCursor.getCount();
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int abcid = managedCursor.getColumnIndex(CallLog.Calls.PHONE_ACCOUNT_ID);

        sim1query = "";
        sim2query = "";
        Textquery=  "";
        sim1query = sim1query + "]";
        sim2query = sim2query + "]";
        tempquery1 = "";
        tempquery1 = tempquery1 + "]";
        tempquery2 = "";
        tempquery2 = tempquery2 + "]";



        while (managedCursor.moveToNext()) {
            int simtype = managedCursor.getInt(abcid);
            final String phNum = managedCursor.getString(number);
            final int temp_num = managedCursor.getInt(number);
            Integer callTypeCode = managedCursor.getInt(type);
            String strcallDate = managedCursor.getString(date);
            Date fullDate = new Date(Long.valueOf(strcallDate));
            String TempDatetime = fullDate.toString();
            final String callTime = new String(TempDatetime.substring(11, 19));
            String monthofdate = new String(TempDatetime.substring(4,7));
            String monthnum = "00";
            String dayofdate = new String(TempDatetime.substring(8,10));
            String daynum = "00";
            switch (monthofdate.toLowerCase()){

                case "jan" :
                    monthnum = "01";
                    break;

                case "feb" :
                    monthnum = "02";
                    break;

                case "mar" :
                    monthnum = "03";
                    break;

                case "apr" :
                    monthnum = "04";
                    break;

                case "may" :
                    monthnum = "05";
                    break;

                case "jun" :
                    monthnum = "06";
                    break;

                case "jul" :
                    monthnum = "07";
                    break;

                case "aug" :
                    monthnum = "08";
                    break;

                case "sep" :
                    monthnum = "09";
                    break;

                case "oct" :
                    monthnum = "10";
                    break;

                case "nov" :
                    monthnum = "11";
                    break;

                case "dec" :
                    monthnum = "12";
                    break;

                default :
                    monthnum = "00";
                    break;

            }

            if (dayofdate.length()==1){
               daynum = "0" + dayofdate;
            }else{
                daynum = dayofdate;
            }
//            final String callDate = new String(TempDatetime.substring(4, 7) + "-" + TempDatetime.substring(9, 10) + "-" + TempDatetime.substring(30, 34));
            final String callDate =TempDatetime.substring(30, 34) + "-" + monthnum + "-" + daynum ;


            int lengthofnum = phNum.length();

            if(simtype == 1){
            kloghandler(simtype, callTypeCode, lengthofnum, i1, phNum, callTime, callDate);
                i1 = i1 + 1;
            }
            if(simtype == 2){
                kloghandler(simtype, callTypeCode, lengthofnum, i2, phNum, callTime, callDate);
                i2 = i2 + 1;
            }


        }
        managedCursor.close();



        tempquery1 = tempquery1 + "[";
        tempquery1 = tempquery1 + "&phnum=" + phonenum1;

        tempquery2 = tempquery2 + "[";
        tempquery2 = tempquery2 + "&phnum=" + phonenum2;


        logcount = i1 + i2;
    }


    public static void webconnection(String templink) {

        webPage ="";
        data="";
        if(templink.isEmpty()){
            templink = "http://aayo.xyz/api/MissCallAayo/post.php?d=";

        }
        final String finalTemplink = templink;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL(finalTemplink);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.connect();
                    InputStream is = urlConnection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    while ((data = reader.readLine()) != null) {
                        webPage = webPage + data;
                    }
                } catch (Exception e) {

                    webPage = e.toString();
                }
            }
        });
        thread.start();

    }


    public void Bgservice() {

        create();


        final SharedPreferences loginfo = getApplicationContext().getSharedPreferences("loginfo", MODE_PRIVATE);
        Link1 = loginfo.getString("Link1", "");
        Link2 = loginfo.getString("Link2", "");

        sim1string ="";
        String sim1string = (Link1 + tempquery1);
        Toast.makeText(MyService.this,"log Limit of Sim1 : ( " +templimit1 + " - " + limit1+ " ) " + sim1string, Toast.LENGTH_SHORT).show();
        webconnection(sim1string);

        sim2string ="";
        String sim2string = (Link2  + tempquery2);
        Toast.makeText(MyService.this,"log Limit of Sim2 : ( " +templimit2 + " - " + limit2 + " ) " + sim2string, Toast.LENGTH_SHORT).show();
        webconnection(sim2string);


    }


    Runnable runnable = new Runnable() {

        public void run() {

            Bgservice();


            handler.postDelayed(this, 300000);



        }
    };


    public void kloghandler(int tempsim, int tempctc, int templon,int tempid, String tempnum, String temptime, String tempdate){

        final SharedPreferences loginfo = getApplicationContext().getSharedPreferences("loginfo", MODE_PRIVATE);
        Link1 = loginfo.getString("Link1", "");
        Link2 = loginfo.getString("Link2", "");

        if(limit1 == 0){ limit1 = limit1 + 10;}
        if(limit2 == 0){ limit2 = limit2 + 10;}

        if((tempsim == 1)&&(tempid>limit1)){

            tempquery1 = tempquery1 + "[";
            tempquery1 = tempquery1 + "&phnum=" + phonenum1;
            sim1string ="";
            String sim1string = (Link1 + tempquery1);
            webconnection(sim1string);
            Toast.makeText(MyService.this,"log Limit of Sim1 : ( " +templimit1 + " - " + limit1 + " ) " + sim1string, Toast.LENGTH_SHORT).show();
            templimit1 = 0;
            templimit1 = limit1;
            limit1 = limit1 + 10;
            tempquery1 = "";
            tempquery1 = tempquery1 + "]";

        }

        if((tempsim == 2)&&(tempid>limit2)){

            tempquery2 = tempquery2 + "[";
            tempquery2 = tempquery2 + "&phnum=" + phonenum2;
            sim2string ="";
            String sim2string = (Link2  + tempquery2);
            webconnection(sim2string);
            Toast.makeText(MyService.this,"log Limit of Sim2 : ( " +templimit2 +" - " + limit2 + " ) " + sim2string, Toast.LENGTH_SHORT).show();
            templimit2 = 0;
            templimit2 = limit2;
            limit2 = limit2 + 10;
            tempquery2 = "";
            tempquery2 = tempquery2 + "]";
        }

        if(tempsim == 1) {

            if ((tempctc == 3) && (templon == 10)) {
                sim1query = sim1query + ("[|" + tempid + "|" + tempnum + "|" + temptime + "|" + tempdate + "|]");
                tempquery1 = tempquery1 + ("[|" + tempid + "|" + tempnum + "|" + temptime + "|" + tempdate + "|]");
                Textquery = Textquery + ("\n \n id: " + tempid + "\n Number : " + tempnum + "\n Time :" + temptime + "\n Date : " + tempdate + "\n Sim no :" + "Sim 1");


            }

            if ((tempctc == 3) && (templon == 14)) {

                sim1query = sim1query + ("[|" + tempid + "|" + tempnum.substring(4) + "|" + temptime + "|" + tempdate + "|]");
                tempquery1 = tempquery1 + ("[|" + tempid + "|" + tempnum.substring(4) + "|" + temptime + "|" + tempdate + "|]");
                Textquery = Textquery + ("\n \n id: " + tempid + "\n Number : " + tempnum.substring(4) + "\n Time :" + temptime + "\n Date : " + tempdate + "\n Sim no :" + "Sim 1");


            }


        }

        if(tempsim == 2) {
            if ((tempctc == 3) && (templon == 10)) {
                sim2query = sim2query + ("[|" + tempid + "|" + tempnum + "|" + temptime + "|" + tempdate + "|]");
                tempquery2 = tempquery2 + ("[|" + tempid + "|" + tempnum + "|" + temptime + "|" + tempdate + "|]");
                Textquery = Textquery + ("\n \n id: " + tempid + "\n Number : " + tempnum + "\n Time :" + temptime + "\n Date : " + tempdate + "\n Sim no :" + "Sim 2");


            }

            if ((tempctc == 3) && (templon == 14)) {

                sim2query = sim2query + ("[|" + tempid + "|" + tempnum.substring(4) + "|" + temptime + "|" + tempdate + "|]");
                tempquery2 = tempquery2 + ("[|" + tempid + "|" + tempnum.substring(4) + "|" + temptime + "|" + tempdate + "|]");
                Textquery = Textquery + ("\n \n id: " + tempid + "\n Number : " + tempnum.substring(4) + "\n Time :" + temptime + "\n Date : " + tempdate + "\n Sim no :" + "Sim 2");


            }


        }






    }


    private final BroadcastReceiver miscallreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
           if(action.equals(android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED)){

               Bgservice();
                //action for phone state changed
            }
        }
    };



}